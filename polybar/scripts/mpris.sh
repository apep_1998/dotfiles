#!/bin/bash

# Get status of media players supporting MPRIS DBUS interface.
#
# Author: Ali Mousavi <ali.mousavi@gmail.com>
# Last Updated: 2018/07/25
#
# Dependencies:
#   - playerctl

ICON_PLAYING=""
ICON_PAUSED=""
FORMAT_PLAYING="%{F#3fa530}"   # Foreground color of the icon while playing.
FORMAT_PAUSED="%{F#a9af2d}"    # Foreground color of the icon while paused.

player_status=$(playerctl status 2> /dev/null)
metadata="$(playerctl metadata artist) - $(playerctl metadata title)"

# Foreground color formatting tags are optional
if [[ ${player_status} = "Playing" ]]; then
    echo "${FORMAT_PLAYING}${ICON_PLAYING}%{F-} ${metadata}"
elif [[ ${player_status} = "Paused" ]]; then
    echo "${FORMAT_PLAYING}${ICON_PAUSED}%{F-} ${metadata}"
else
    echo ""
fi

