# ~/.zshrc.local
# Customizations to zshell. based on grml-zsh-config
#
# Author: Ali Mousavi
# Last Updated: 2019/09/14
#
# Dependencies:
#   - grml-zsh-config
#   - htop
#   - pkgfile (for command not found hook in Arch Linux)
#   - source-highlight (source code highlight in less)   
#   - jcal-git (for jalali date in prompt)
#   - zsh-syntax-highlighting
#   - zsh-autosuggestions
#   - zsh-history-substring-search

#----------------------------------------------------------------------
#  Preperation
#----------------------------------------------------------------------

# Load some personal secret environment variables.
if [ -f ~/.zshrc.secret  ]; then
  source ~/.zshrc.secret
fi
# enable vim mode
bindkey -v

#----------------------------------------------------------------------
#  CUSTOM ALIASES
#----------------------------------------------------------------------

# Modified Commands:
alias grep='grep --color=auto'   # Colorful grep
alias diff='diff --color=auto'   # Colorful diff
alias df='df -h'                 # Human readable size
alias du='du -c -h'              # Human readable size and calculate grand total
alias ls='ls -h --color=auto'    # Colorfull and Human readable size
alias mkdir='mkdir -v'           # Verbose mode
alias tmux='tmux -2'             # Enables 256 Colors in tmux

# New Commands
alias pi='ping 8.8.4.4'          # Ping Google's DNS
alias top='htop'                 # Use htop instead of top (needs htop package)
alias du1='du --max-depth=1'     # Compute size of current directory.
alias ducks='du -cks * | sort -rn | head -20' # 20 largest files & Dirs
# alias i3rc='vim ~/.i3/config'
# alias aria2rpc="ruby /usr/share/doc/aria2/xmlrpc/aria2rpc --secret ${ARIA2RPC_SECRET}"
# alias netreset='sudo systemctl restart netctl-auto@wlp3s0.service'
alias reboot='systemctl reboot'
alias poweroff='systemctl poweroff'

# charge the battery to full/half capacity (Specific to some IdeaPad/ThinkPad 
# Models)
if (( UID != 0 )); then
  alias batteryhalf='echo "\_SB.PCI0.LPCB.EC0.VPC0.SBMC 3" | sudo tee /proc/acpi/call'
  alias batteryfull='echo "\_SB.PCI0.LPCB.EC0.VPC0.SBMC 5" | sudo tee /proc/acpi/call'
else
  alias batteryhalf='echo "\_SB.PCI0.LPCB.EC0.VPC0.SBMC 3" > /proc/acpi/call'
  alias batteryfull='echo "\_SB.PCI0.LPCB.EC0.VPC0.SBMC 5" > /proc/acpi/call'
fi

# Privileged Access:
if (( UID != 0 )); then
  alias svim='sudo vim'
  alias root='sudo su'
  alias pacman='sudo pacman'
  alias netctl='sudo netctl'
fi

# Safety is best
alias cp='cp -i'                 # Prompts before overwriting a file.
alias mv='mv -i'                 # Prompts before overwriting a file.
alias rm='rm -I'                 # Asks before deleting (-i ask for every file)
alias ln='ln -i'                 # Prompts before overwriting a file.
alias chown='chown --preserve-root' # Don't operate recursively on "/"
alias chmod='chmod --preserve-root' # Don't operate recursively on "/"
alias chgrp='chgrp --preserve-root' # Don't operate recursively on "/"

#----------------------------------------------------------------------
#  ENVIRONMENT VARAIBLES
#----------------------------------------------------------------------

export MAKEFLAGS="-j$(nproc)"         # Optimize compling speed based on the number of cpu cores.
export PATH=${PATH}:${HOME}/bin       # add directory for personal scripts
export EDITOR=vim                     # Define default editor
export _JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on' # Better fonts in java apps
export QT_STYLE_OVERRIDE='GTK+'       # QT5 Style
export LC_TIME="en_US.utf8"           # Gnome shows dates in persian. this should fix it.
export ELECTRON_TRASH="gio"           # Change default trash command for electron apps

# Colorfull less
export LESSOPEN="| /usr/bin/source-highlight-esc.sh %s"
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

#----------------------------------------------------------------------
#   PROMPT
#----------------------------------------------------------------------

prompt off
eval "$(starship init zsh)"

## set the prompt theme
#prompt grml

##Virtualenv support
#function virtual_env_prompt () {
#    REPLY=${VIRTUAL_ENV+ (${VIRTUAL_ENV:t}) }
#}
#grml_theme_add_token  virtual-env -f virtual_env_prompt '%B%F{red}' '%f%b'

##Jalali date support
#function jalali_date_prompt {
#    REPLY=" ($(jdate "+%F"))"
#}
#grml_theme_add_token  jdate -f jalali_date_prompt '%F{yellow}' '%f'

#zstyle ':prompt:grml:left:setup' items rc history shell-level time date jdate virtual-env change-root newline change-root user at host path vcs percent

## Coloring the prompt
#zstyle ':prompt:grml:*:items:user' pre '%B%F{cyan}'
#zstyle ':prompt:grml:*:items:user' post '%f%b'
#zstyle ':prompt:grml:*:items:at' pre '%F{green}'
#zstyle ':prompt:grml:*:items:at' post '%f'

#----------------------------------------------------------------------
#  Plugins
#----------------------------------------------------------------------
# Command not found hook for Arch Linux (requires pkgfile)
source /usr/share/doc/pkgfile/command-not-found.zsh

# Fish like suggestions (requires https://github.com/zsh-users/zsh-autosuggestions)
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# ZSH syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# ZSH fish like history search (requires zsh-history-substring-search)
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
bindkey '^[OA' history-substring-search-up
bindkey '^[OB' history-substring-search-down

