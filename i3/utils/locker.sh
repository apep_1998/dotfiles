#!/bin/bash

# locker.sh
# Send notification and lock the screen using xautolock and i3lock
#
# Author: Ali Mousavi <ali.mousavi@gmail.com>
# Last Updated: 2018/07/24
# 
# Dependencies:
#   - xautolock
#   - i3lock
#   - dunst

# Using xautolock and i3lock, totally simpler and more beautiful than
# xscreensaver. Initially used this guide:
# http://rabexc.org/posts/awesome-xautolock-battery

lock_min="3" # Minutes of inactivity before locking the screen. [1 <= mins <= 60]
notify_sec="30" # Seconds before the screen lock to warn the user (using dunst)
notification_msg="LOCKING the screen in 30 Seconds"

exec xautolock -detectsleep \
  -time ${lock_min} \
  -locker "pkill -u $USER -USR1 dunst; ~/.i3/utils/i3_lock_command.sh; pkill -u $USER -USR2 dunst" \
  -notify ${notify_sec} \
  -notifier "notify-send -u critical -- ${notification_msg}" \
  2>/dev/null 1>&2 &

