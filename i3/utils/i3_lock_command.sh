# i3_lock_command.sh
# locks the screen using i3lock-color
#
# Author: Ali Mousavi <ali.mousavi@gmail.com>
# Last Updated: 2020/08/26
# 
# Dependencies:
#   - i3lock-color

i3lock -n \
       --clock \
       --blur 8 \
       --keylayout 2 \
       --insidecolor=00000000 \
       --ringcolor=ffffffff \
       --line-uses-inside \
       --keyhlcolor=d23c3dff \
       --bshlcolor=d23c3dff \
       --separatorcolor=00000000 \
       --insidevercolor=fecf4dff \
       --insidewrongcolor=d23c3dff \
       --ringvercolor=ffffffff \
       --ringwrongcolor=ffffffff \
       --timecolor=ffffffff \
       --datecolor=ffffffff
