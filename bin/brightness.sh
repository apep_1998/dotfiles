#! /bin/bash

# brightness.sh
# set the brightness of screen.
#
# usage: brightness.sh [low?]
#
# Author: Ali Mousavi
# Last Updated: 2018/07/25
#
# I use this script to set the brightness of my screen from udev
# events on battery and AC.

BATTERY="600"
AC="1300"

if [[ "$1" = "low" ]]; then
  echo -n ${BATTERY} > /sys/class/backlight/intel_backlight/brightness
else
  echo -n ${AC} > /sys/class/backlight/intel_backlight/brightness
fi

