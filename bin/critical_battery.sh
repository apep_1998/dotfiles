#! /bin/bash
#
# critical_battery.sh
# Send warning notification and hibernate the system based on the 
# battery level and state.
#
# Author: Ali Mousavi
# Last Updated: 2018/07/25
#

level_warn=10
level_hibernate=5

acpi -b | awk -F'[,:%]' '{print $2, $3}' | {
  read -r status capacity

  if [ "$status" = Discharging -a "$capacity" -lt "$level_warn" ]; then
    /home/ali/bin/root_notify.sh "Battery Low (${capacity}%)" "Connect an AC Adapter" "battery-low"
  fi

  if [ "$status" = Discharging -a "$capacity" -lt "level_hibernate" ]; then
    logger "Critical battery threshold, hibernating..."
    systemctl hibernate
  fi
}
