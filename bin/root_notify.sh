#!/bin/bash
#
# udev-notify.sh
# Send notifications from a root process to the main user.
#
# Author: Ali Mousavi (ali.mousavi@gmail.com)
# Last Update: 2018/07/23
#
# Requirements:
#   - A notification daemon
#   - notify-send

# SETTINGS
CURRENT_DISPLAY=":0"

notify() {
  local header="$1"
  local message="$2"
  local icon="$3"

  # assume there's only one user logged (except root)
  local logged_user=$(who | grep -v root | cut -d' ' -f1 | uniq)
  local user_id=$(id -u "${logged_user}")

  # show the notification as the user.
  export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/${user_id}/bus"
  export DISPLAY=${CURRENT_DISPLAY}
  su $logged_user -c "notify-send -i \"${icon}\" \"${header}\" \"${message}\""
}

notify "$1" "$2" "$3"
