#! /bin/bash

# pacman_backup.sh
# Backup/Sync pacman package lists to a location

# Author: Ali Mousavi
# Last Updated: 2018/11/10
#

#--------------------
# Configuration:
#--------------------
BACKUP_DIR="/home/ali/Documents/Backup/Pacman"
LOG_FILE="/home/ali/Documents/Backup/backup.log"
USER=ali
GROUP=ali

mkdir -p "${BACKUP_DIR}"
/usr/bin/pacman -Qenq > "${BACKUP_DIR}/native_packages.txt"
/usr/bin/pacman -Qemq > "${BACKUP_DIR}/aur_packages.txt"

if (( $?  )); then
  echo 'An error occured while trying to backup pacman package lists'
  echo "Pacman: $(date "+%F-%T") - Something happened" >> ${LOG_FILE}
  return 1
fi
echo "Finished backing up pacman package lists."
echo "Pacman: $(date "+%F-%T")" >> ${LOG_FILE}

# Clean Up
chown -R ${USER}:${GROUP} ${BACKUP_DIR}
