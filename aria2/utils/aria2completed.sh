#!/usr/bin/bash

# aria2completed.sh
# Send notification when a download is finished and move the files
# to another folder
#
# Author: Ali Mousavi
# Last Updated: 2018/11/14
#
# Dependencies:
#   - curl
#   - python3
#
# Usage: You need to put this line in your aria2c configuration file:
#        on-download-complete=/path/to/aria2completed.sh

# In case of torrents, the name of the first file will be sent instead of the
# torrent name. Therefore we use RPC to retrive the torrent name.
ENABLE_RPC="true"
RPC_ADDRESS="http://127.0.0.1:6800/jsonrpc"
RPC_SECRET="$ARIA2RPC_SECRET"
ARIA2_DIR="/home/ali/Downloads/aria2"

rpc_name() {
  if [ -z ${RPC_SECRET} ]; then
    echo "RPC secret not provided."
    exit 1
  fi

  GID="$1"
  JSON=$(curl -s "${RPC_ADDRESS}" \
        -H "Content-Type: application/json" \
        -H "Accept: application/json" \
        --data "{\"jsonrpc\": \"2.0\",\"id\":1, \"method\": \"aria2.tellStatus\", \"params\":[\"token:$RPC_SECRET\", \"${GID}\", [\"bittorrent\"] ]}")
  
  echo ${JSON} | python3 -c "import sys, json; print(json.load(sys.stdin)['result']['bittorrent']['info']['name'])" 2>/dev/null
}

notify() {
  if [ $2 -eq "0" ]; then
    # Downloading torrent magnet metadata will report 0 files and is not 
    # something we want to track.
    exit 0
  fi

  if [ "$2" -eq "1" ] || [ ${ENABLE_RPC} = "false" ]; then
    DOWNLOAD_NAME=$(basename $3)
  else
    DOWNLOAD_NAME=$(rpc_name $1)
  fi

  # Send notification to android
  /home/ali/bin/andropush "${DOWNLOAD_NAME}" "Aria2: ${2} File(s) Downloaded"

  # Send Desktop Notification
  export Display=":0"
  export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus"
  notify-send -i "emblem-downloads" "Aria2: ${2} File(s) Downloaded" "${DOWNLOAD_NAME}"
}

move_downloads() {
  # Do nothing if no files.
  if [ "$2" == "0" ]; then
    exit 0
  fi

  # Get the file path.
  SRC=$3

  # Loop through the file path, find until parent directory matching "running".
  while [ "$SRC" != "$ARIA2_DIR" ]; do
    DIR=$(dirname "$SRC")
    if [ $(basename "$DIR") == 'downloading' ]; then
      # Move the file/directory to "complete" folder.
      mv "$SRC" "$ARIA2_DIR"
      exit $?
    else
      SRC=$DIR
    fi
  done
}

notify "$1" "$2" "$3"
move_downloads "$1" "$2" "$3"

