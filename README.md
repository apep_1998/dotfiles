# Dotfiles

![i3 + Polybar](assets/images/desktop.png)

These are a collection of my configuration files and scripts I use on my machine. My laptop is powered by Arch Linux and I currently use i3 + polybar as my preferred Window Manager/DE.

Installation of these files are possible using [dotbot](https://github.com/anishathalye/dotbot). for **Installation Instruction,** see below.

## Table of Contents

[TOC]

## Configuration Files

### Zsh

![zsh prompt preview](assets/images/zsh.png)

- Based on [grml-zsh-config](https://grml.org/zsh/)
- Two line prompt with support for git and virtualenv
- Jalali (Persian) date in prompt
- Pacman "command  not found" hook
- Fish like suggestions
- Fish like history search
- Syntax highlighting
- Colorfull man pages

### Vim

![Vim](assets/images/vim.png)

- Easy installation. Just copy the file over to ~/.vimrc and open vim. (Check the [dependencies](#vim-1))
- 2 space indentation.
- Case insensitive search.
- Using [vim-plug](https://github.com/junegunn/vim-plug) as plugin manager.
- Mostly enhanced for JavaScript development. 
- Plugins Included:
  - [vim-commentary](https://github.com/tpope/vim-commentary): Comment stuff out.
  - [ctrlp.vim](https://github.com/kien/ctrlp.vim): Full path fuzzy file, buffer, mru, tag, ... finder for Vim.
  - [vim-airline](https://github.com/vim-airline/vim-airline): lean & mean status/tabline for vim that's light as air.
  - [vim-colorschemes](https://github.com/flazz/vim-colorschemes): one colorscheme pack to rule them all!
  - [syntastic](https://github.com/vim-syntastic/syntastic): a syntax checking plugin.
  - [emmet-vim](https://github.com/mattn/emmet-vim): a vim plug-in which provides support for expanding abbreviations similar to [emmet](http://emmet.io/).
  - [tern_for_vim](https://github.com/ternjs/tern_for_vim): *Tern*-based JavaScript editing support.
  - [YouCompleteMe](https://github.com/Valloric/YouCompleteMe): fast, as-you-type, fuzzy-search code completion engine for Vim.
  - [vim-snipmate](https://github.com/garbas/vim-snipmate): support for textual snippets.
  - [vim-snippets](https://github.com/honza/vim-snippets): vim-snipmate default snippets.
  - [nerdtree](https://github.com/scrooloose/nerdtree): A tree explorer plugin for vim. 
  - [vim-gitgutter](https://github.com/airblade/vim-gitgutter): A Vim plugin which shows a git diff in the gutter (sign column) and stages/undoes hunks. 
  - [vim-fugitive](https://github.com/tpope/vim-fugitive): A Git wrapper so awesome, it should be illegal.
  - [vim-surround](https://github.com/tpope/vim-surround): quoting/parenthesizing made simple.
  - [auto-pairs](https://github.com/jiangmiao/auto-pairs): Insert or delete brackets, parens, quotes in pair.
  - [auto-format](https://github.com/Chiel92/vim-autoformat): Format code with one button press.
  - [vim-jsdoc](https://github.com/heavenshell/vim-jsdoc): Generate JSDoc to your JavaScript code. 
  - [vim-javascript](https://github.com/pangloss/vim-javascript): JavaScript bundle for vim, this bundle provides syntax highlighting and improved indentation.
  - [vim-jsx](https://github.com/mxw/vim-jsx): Syntax highlighting and indenting for JSX.
  - [vim-node](https://github.com/moll/vim-node): Tools to make Vim superb for developing with Node.js.
  - [javascript-libraries-syntax.vim](https://github.com/othree/javascript-libraries-syntax.vim): Syntax for JavaScript libraries.
  - [vim-indent-guides](https://github.com/nathanaelkane/vim-indent-guides): Indent Guides is a plugin for visually displaying indent levels in Vim.
  - [vim-pug](https://github.com/digitaltoad/vim-pug): Vim syntax highlighting for Pug (formerly Jade) templates.
  - [vim-pug-complete](https://github.com/dNitro/vim-pug-complete): Vim omni-completion support for pug (formerly jade) template engine.
  - [vim-coffee-script](https://github.com/kchmck/vim-coffee-script): CoffeeScript support for vim.
  - [vim-rooter](https://github.com/airblade/vim-rooter): Changes Vim working directory to project root (identified by presence of known directory or file). 
  - [jspc.vim](https://github.com/othree/jspc.vim): JavaScript Parameter Complete.
  - [vim-prettier](https://github.com/prettier/vim-prettier): A vim plugin wrapper for prettier, pre-configured with custom default prettier settings.

### i3-gaps

- [Dynamic Workspace Switching](https://i3wm.org/docs/user-contributed/workspace-current-output.html) (`switch-workspace.py`). 
- Keyboard multimedia keys (requires playerctl).
- `Win + d` to open rofi.
- `Win + x` to open rofi-pass.
- `1px` Border with no window title bar.
- 2 Monitor support.
- Adjust monitor configurations on startup using an external script (`screens.sh`).
- Startup Applications:
  - Polybar (Menu bar)
  - i3lock-color (screenlock) - configured in `locker.sh`
  - xautolock (screenlock) - configured in `locker.sh`
  - setxkbmap (keyboard layout configuration)
  - lxqt-policykit-agent (A simple policykit agent)
  - picom (a simple compositing manager)
  - dunst (a simple notification daemon)
  - feh (Set a background)
  - redshift (Adjusts the color temperature of your screen according to your surroundings.)
  - parcellite (clipboard manager)
  - A custom startup script to open some programs based on the ESSID of the wifi (`i3_startup.sh`).

### Polybar

- Two bars (top and bottom)
- Icons powered by Font Awesome 5
- Default font is Zekton
- modules:
  - i3 workspaces
  - focused window title
  - xbacklight
  - volume bar
  - keyboard layout
  - battery status
  - date
  - shutdown menu
  - mpris music player status
  - network speed meter
  - filesystem usage
  - memory usage
  - CPU usage
  - CPU temprature
  - GPU temprature
  - Pacman updates
  - vpn connection status
  - WIFI status
  - Jalali date

### Aria2

![AriaNg](assets/images/ariang.png)

- 2 configuration files (for RPC daemon and command line mode)
- Script to notify when a download is finished in RPC mode, and change the directory after the download is completed. (`/aria2/utils/aria2completed.sh`)

RPC mode can be enabled using a systemd daemon. eg. create the `/etc/systemd/system/aria2.service` file with the following content:

```ini
[Unit]
Description=aria2c download manager
After=network.target

[Service]
User=%YOUR_USERNAME%
Type=forking
Environment="PUSHBULLET_ACCESS_TOKEN=%YOUR_PUSHBULLET_ACCESS_TOKEN%"
Environment="ARIA2RPC_SECRET=%YOUR_RPC_SECRET%"
ExecStart=/usr/bin/aria2c --rpc-secret=%YOUR_RPC_SECRET% --conf-path=/home/%YOUR_USERNAME%/.aria2/aria2.daemon

[Install]
WantedBy=multi-user.target
```

**Note** that the values of `%YOUR_USERNAME%` , `%YOUR_PUSHBULLET_ACCESS_TOKEN%` and `%YOUR_RPC_SECRET%` should be replaced by your information. In case you don't need notifications on your phone you can omit the Push Bullet information.

The systemd daemon should be enabled/started to run the aria2 RPC daemon.

To manage your downloads using the RPC interface, you can choose between many different aria2 RPC user interfaces available. eg. [AriaNg](https://github.com/mayswind/AriaNg)

To schedule your downloads (start and stop them on certain times) a systemd timer/cronjob can be used. Let me know if you need help with that. 

### Fontconfig (Optimized Persian Fonts)

- Rule out bitmap fonts
- Use *Vazir* font for sans-serif (only in persian text)
- Use *Iranian Serif* for serif (only in persian text)
- Use *Vazir Code* for monospaced persian texts.
- Use *Vazir* font instead of *Arial* in persian texts.

### Misc Files

- tmux.conf
- xinitrc
- termite
- rofi
- rofi-pass
- rtorrent.rc
- redshift
- eslintrc

## Script Files

### bin/andropush 

Push notifications to an Android phone using [Pushbullet](https://www.pushbullet.com/) API.

usage: `andropush <message-body> [header?]`

Requires Access-Token to be set using `PUSHBULLET_ACCESS_TOKEN` environment variable, or in a file in the same directory of the script named `.andropush_env`. You can also set it inside the script.

### bin/archbackup

A script I use to backup my data to my external hard disk. It supports taking a backup from:

- Installed package lists
- /etc folder
-  customizable files and folders in /home
- mysql databases

**Note** that this script uses `rsync` to synchronize the folders from your system to the backup location, therefore it might take a lot of time to complete on the first run based on the amount of data you choose to synchronize, But the time is going to be much less from the second time and on.

**Also note** that the script is smart enough to check for the filesystem of the destination. If your destination is *NTFS* or *FAT*, the ownership and the permission data of your files can't be preserved.

The configuration is pretty easy and self explanatory.

### bin/brightness.sh

Set brightness level of the screen either to low or high.

usage: `brightness [low?]`

The script can be bound to a udev event to set the screen brightness on battery or AC. here is how I set the brightness level in `/etc/udev/rules.d/99-battery.rules`:

```
SUBSYSTEM=="power_supply", ATTR{status}=="Discharging", RUN+="/home/ali/bin/brightness.sh low"
ACTION=="change", SUBSYSTEM=="power_supply", ATTR{online}=="1", RUN+="/home/ali/bin/brightness.sh"
```

### bin/conv2mkv

Converts videos to mkv using `x264` or`x265` or any codec you specify.

usage: 

convert a single file: `conv2mkv [filename]` 

convert the files in the current folder: `conv2mkv -d`

convert the files in the current folder and all its subfolders: `conv2mkv -dr`

**Note** that by default it tries to convert audio using the `libfdk_aac` codec which will fail in some of the linux distributions. you can either compile `ffmpeg` with libfdk_aac codec or install it from [AUR](https://aur.archlinux.org/packages/ffmpeg-libfdk_aac/) if you use Archlinux. Or simply change the codec to `aac` in the script.

Dependecies:

- ffmpeg (compiled with libffmpeg_aac)
- pv

### bin/critical_battery.sh

Send warning notification and hibernate the system based on the battery level and state.

I use this one along with a systemd timer and a udev rule to pull the battery state each minute and warn the user or put the system on hibernate when the battery level is critical.

### bin/mirrors-update

Automates the process of merging the `mirrorlist.pacnew` files with the `mirrorlist` file in Arch Linux. Rates the mirrors and sort them, it will also enables delta mirrors if required. Requires `reflector` package.

### bin/pacdiff

This script checks the `/etc` folder for any file with the `pacnew` extension and offers different options to deal with them. Usefull only in Arch Linux

### bin/root_notify.sh

Set environment and send desktop notifications from root processes.

usage:  `root_notify.sh <header> <message> [icon-name?]`

### bin/rebouter

a simple script to restart the adsl connection of my old router at home. requires `expect` and `telnet`. the script expects that the router password is set with the `$ROUTER_PASS` environment variable.

### bin/wintersmith-post

A script to start an article in the wintersmith static blog generator.

## Dependencies

### Zsh

-  [grml-zsh-config](https://grml.org/zsh/) (package available in Arch Linux)
- htop (since I alias top to htop)
- pkgfile (for command not found hook in Arch Linux)
- [jcal](http://www.nongnu.org/jcal/) (for Jalali date in Prompt) (Package available in [AUR](https://aur.archlinux.org/packages/jcal-git/))
- [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting/) (Package available in Arch Linux)
- [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions) (Package available in Arch Linux)
- [zsh-history-substring-search](https://github.com/zsh-users/zsh-history-substring-search) (Package available in Arch Linux)

### Vim

- python3
- python3-dev (On debian-based)
- cmake (Compiling YouCompleteMe plugin)
- npm (Installing tern and vim-prettier)

### i3-gaps

- Polybar (menu bar)
- rofi (launch application using `mod + d`)
- i3lock-color (locking the screen)
- xautolock (locking the screen based on inactivity)
- setxkbmap (changing the keyboard layout)
- lxqt-policykit-agent (Policykit Agent)
- picom (Compositing support)
- dunst (Notification Daemon)
- feh (Setting the background)
- redshift (Adjusts the color temperature of your screen according to your surroundings.)
- parcellite (clipboard manager)
- playerctl (Multimedia keys on the keyboard)
- wireless_tools (run the scripts based on ESSID of the network)

### Polybar

- Ubuntu Condensed font (ttf-ubuntu-family package in Arch Linux)
- [Icomoon](https://icomoon.io/icons-icomoon.html)
- Vazir font: font used in Jalali date module. (Package available in [AUR](https://aur.archlinux.org/packages/vazir-code-fonts/))
- libnl or wireless_tools: Network related modules.
- playerctl: MPRIS media players module
- xbacklight: get and set the screen backlight.
- pacman-contrib: check Arch updates.
- jcal: Jalali date module. (Package available in [AUR](https://aur.archlinux.org/packages/jcal-git/))
- fribidi: Jalali date module.

### Fontconfig

- [Vazir](https://github.com/rastikerdar/vazir-font) font (Available in [AUR](https://aur.archlinux.org/packages/vazir-fonts/))
- [Vazir Code](https://github.com/rastikerdar/vazir-code-font) font (Available in [AUR](https://aur.archlinux.org/packages/vazir-code-fonts/))
- [Iranian Serif](https://fontlibrary.org/en/font/iranian-gui-fonts) font (Available in [AUR](https://aur.archlinux.org/packages/iranian-fonts/))
- [Samim](https://github.com/rastikerdar/samim-font) font (Available in [AUR](https://aur.archlinux.org/packages/samim-fonts/))

## TODO

- Deprecate wireless_tools
- Change font of Jalali date module in polybar (Maybe to Mikhak)
- Add Flexget configurations.



## Installation Instruction

A complete and safe installation of the configuration files is possible (thanks to [dotbot)](https://github.com/anishathalye/dotbot). Note that this method **will not** remove your old configuration files if already available.

Also note that some parts of the configurations has to be changed according to your environment and requirements.

```bash
$ git clone https://gitlab.com/tuxitop/dotfiles.git
$ cd dotfiles
$ ./install.sh
```
